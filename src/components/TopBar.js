import React, { Component } from 'react';

import './TopBar.css';

class TopBar extends Component {
    render() {
        return (
            <nav className="navbar navbar-light bg-light border-bottom sticky-top top-bar navbar-expand-lg">
                <div className="row">
                    <img id="topbar-logo" 
                        src="img/UT-Logo.png" 
                        alt="The University of Tennessee System" 
                        width="318" height="62" />
                </div>
            </nav>
        );
    }
}

export default TopBar;
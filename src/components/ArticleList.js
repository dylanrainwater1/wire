import React, { Component } from 'react';
import Article from './Article';

import './ArticleList.css'

import { Hits, Pagination, ScrollTo, Configure, PoweredBy } from 'react-instantsearch-dom';

class ArticleList extends Component {
    constructor(props) {
        super(props);
        this.Hit = ({ hit }) => <Article data={hit} modalChange={props.modalChange} />;
    }

    render() {

        return (
            <div className="col h-scroll pt-1">
                <Configure hitsPerPage={10} />

                <ScrollTo >
                    <Pagination   
                        showFirst={true}
                        showPrevious={true}
                        showNext={true}
                        showLast={true}
                        padding={3}
                        className='mt-2'/>
                </ScrollTo>

                <ul className="list-unstyled">
                    <Hits hitComponent={this.Hit}/>
                </ul>

                <Pagination   
                        showFirst={true}
                        showPrevious={true}
                        showNext={true}
                        showLast={true}
                        padding={3}
                />
                <PoweredBy className="powered-by"/>
            </div>
        );
    }
}

export default ArticleList;
import React, { Component } from 'react';
import { JSDOM } from 'jsdom';

import './ArticleModal.css';

import { Modal, Button } from 'react-bootstrap';
import LoadingSpinner from './LoadingSpinner';

const firebase = require('firebase');
require('firebase/firestore');

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: 'AIzaSyARwXjdXxlUFbPQCYRpQfBKkNHwyaImPkY',
    authDomain: 'aggregation-app.firebaseapp.com',
    projectId: 'aggregation-app'
});
var db = firebase.firestore();

/* 
    ArticleModal is dual purpose, it:
        1) Fetches long data from Firestore,
        2) Displays in modal
*/
class ArticleModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            content: '',
            htmlContent: '',
            copied: false,
        }

        this.copyToClipboard = this.copyToClipboard.bind(this);
        this.componentWillReceiveProps = this.componentWillReceiveProps.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleShow = this.handleShow.bind(this);
    }

    /* Fetches data for new article */
    componentWillReceiveProps(nextProps) {
        if (!nextProps.id) {
            return;
        }

        this.setState({show: nextProps.show, loading: true});

        db.collection('articles').doc(nextProps.id).get().then((doc) => {
            let htmlContent = this.filter(doc.data().htmlContent);

            this.setState({
                content: doc.data().content,
                loading: false,
                htmlContent: htmlContent,
            });
        }).catch((error) => {
            this.setState({ content: error, loading: false });
        });
    }


    /*  Given an html string as `src`,
        Strips styling from divs and images and returns new html.
        This is used for the modal content, to prevent wonky display.
    */
    filter(src) {
        const element = new JSDOM(src);

        /* Remove styling from images */
        let images = element.window.document.getElementsByTagName('img');
        for (let i in images) {
            if (images[i].style) {
                images[i].removeAttribute('style');
                images[i].removeAttribute('width');
                images[i].removeAttribute('height');
                images[i].removeAttribute('class');
                images[i].removeAttribute('sizes');
                images[i].removeAttribute('srcset');
            }
        }
        
        /* Remove styling from divs */
        let divs = element.window.document.getElementsByTagName('div');
        for (let i in divs) {

            if (divs[i].style) {
                divs[i].removeAttribute('style');
                divs[i].removeAttribute('class');
            }
        }
                
        /* Remove styling from figures */
        let figures = element.window.document.getElementsByTagName('figure');
        for (let i in figures) {
            if (figures[i].style) {
                figures[i].removeAttribute('style');
                figures[i].removeAttribute('class');
            }
        }

        let rv = element.window.document.documentElement.outerHTML;
        if (rv.indexOf('<body></body>') !== -1) {
            return src;
        } else {
            return rv;
        }
    }

    copyToClipboard(e) {
        let dataToCopy = this.state.htmlContent ? this.state.htmlContent : this.state.content;

        e.preventDefault();
        this.copyToClip(dataToCopy);
        this.setState({ copied: true });

        setTimeout(
            function () {
                this.setState({ copied: false });
            }
                .bind(this),
            1001
        );
    }

    copyToClip(str) {
        function listener(e) {
            e.clipboardData.setData("text/html", str);
            e.clipboardData.setData("text/plain", str);
            e.preventDefault();
        }
        document.addEventListener("copy", listener);
        document.execCommand("copy");
        document.removeEventListener("copy", listener);
    };

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    render() {
        return (
            <Modal size="xl" show={this.state.show} onHide={this.props.handleClick}>
                <Modal.Header closeButton>
                <Modal.Title>{this.props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                        {   this.state.loading ?
                                <LoadingSpinner/> :
                            this.state.htmlContent ?
                                <div className="modal-article" dangerouslySetInnerHTML={{ __html: this.state.htmlContent }}></div> :
                                <div className="modal-article modal-article-raw">{this.state.content}</div>
                        }
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={this.props.handleClick}>
                    Close
                </Button>

                <button type="button"
                        className={"btn btn-info copy-btn " + (this.state.copied ? 'copied' : '')}
                        onClick={this.copyToClipboard}>
                        Copy to Clipboard
                </button>

                <a role="button"
                    className="btn btn-primary"
                    href={this.props.url}
                    target='_blank'
                    rel='noopener noreferrer'>Go to website</a>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default ArticleModal;
import React, { Component } from 'react';

/* 
    This components renders a loading spinner until content is ready to be displayed.
*/
class LoadingSpinner extends Component {
    render() {
        var spinners = ['text-primary', 'text-success',
                        'text-danger', 'text-warning', 'text-info',
                       ];
        var spinner_type = spinners[Math.floor(Math.random()*spinners.length)];

        return (
            <div className="text-center">
                <div className={"spinner-border mt-5 " + spinner_type} role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        );
    }
}

export default LoadingSpinner;
import React, { Component } from 'react';
import algoliasearch from 'algoliasearch/lite';
import { InstantSearch } from 'react-instantsearch-dom';

import ArticleList from './ArticleList';
import TopBar from './TopBar';
import SideBar from './SideBar';
import ArticleModal from './ArticleModal';

/* Used for search API */
const client = algoliasearch('LFBVCH9W02', 'f3fd9d1c0f827ac7f72bbb06be255f7b');
const indexName = 'articles';

/* Main component */
class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '',           /* This data used for Modal fetch */
            objectID: '',        /* This data used for Modal fetch */
            url: '',             /* This data used for Modal fetch */
            showModal: false,    /* Whether modal should be displayed or not */
        };

        this.modalChange = this.modalChange.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    /* Changes data in modal object */
    modalChange(title, objectID, url) {
        this.setState({
            title, objectID, url,
            showModal: true,
        });
    }

    closeModal() {
        this.setState({ showModal: false, });
    }

    render() {
        return (
            <div>
                <InstantSearch searchClient={client} indexName={indexName}>
                    <TopBar />

                    <div className='row'>
                        <SideBar />
                        <ArticleList modalChange={this.modalChange} />
                        <ArticleModal title={this.state.title}
                            id={this.state.objectID}
                            url={this.state.url}
                            show={this.state.showModal}
                            handleClick={this.closeModal}
                        />
                    </div>
                </InstantSearch>
            </div>
        );
    }
}

export default App;
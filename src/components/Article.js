import React, { Component } from 'react';
import './Article.css';

class Article extends Component {
    constructor(props) {
        super(props);

        this.modalChange = this.modalChange.bind(this);
    }

    getThumbnail(source) {
        const translation = {
            /* Specific */
            'UT System': 'UT-System',
            'IPS': 'IPS',
            'UTHSC': 'UTHSC',
            'UTC': 'UTC',
            'UTIA': 'UTIA',
            'UTM': 'UTM',
            'Alumnus': 'Tennessee-Alumnus',
            'Torchbearer': 'UTK-Torchbearer',
            'UTK': 'UTK-signature-Smokey-Gray',
            'UTRF': 'UTRF',
            'Campus Scene': 'UTM',
            /* Generic UTK */
            'ArtSci': 'UTK-power-TSmokey-Gray',
            'ArchDesign': 'UTK-power-TSmokey-Gray',
            'Tickle': 'UTK-power-TSmokey-Gray',
            'Quest': 'UTK-power-TSmokey-Gray',
            'Nursing': 'UTK-power-TSmokey-Gray',
            'Law': 'UTK-power-TSmokey-Gray',
            'CEHHS': 'UTK-power-TSmokey-Gray',
        };
        
        const imgPic = translation[source];

        return `img/${imgPic}.png`;
    }

    modalChange() {
        this.props.modalChange(this.props.data.title, this.props.data.objectID, this.props.data.url);
    }

    render() {
        if (!this.props.data) return;
        let { thumbnail, title, excerpt, published, source } = this.props.data;
        thumbnail = thumbnail ? thumbnail : this.getThumbnail(source);

        if (published && published.seconds) {
            published = toDateTime(published.seconds);
        } else if (published) {
            published = toDateTime(published._seconds);
        }

        excerpt = excerpt.trim();
        excerpt = excerpt.replace('read more', '');
        excerpt = excerpt.replace('Read more', '');
        excerpt = excerpt.replace('Read More', '');
        excerpt = excerpt.replace('→', '');
        excerpt = excerpt.replace('read', '');
        excerpt = excerpt.trim();

        let source_class = source.toLowerCase();
        if (source === 'UT System') source_class = 'system';

        var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

        return (
            <div className="blog-card bg-light">
                <div className="meta">
                    <div className="photo" 
                         style={{'backgroundImage': `url(${thumbnail})`}}
                         onClick={this.modalChange}>
                    </div>
                </div>
                <div className="description">
                    <button className='btn-title'
                            onClick={this.modalChange} >
                            <h1>{title}</h1>
                    </button>
                    <h2><i>{published ? published.toLocaleString('en-US', options) : ''}</i></h2><span className={"label " + source_class}>{source}</span>
                    <p>{excerpt}</p>
                    <p className="read-more">
                        <button type="button"
                                className="btn btn-primary" 
                                onClick={this.modalChange} >
                            Read Article
                        </button>
                    </p>
                </div>
            </div>
        );
    }
}

function toDateTime(sec) {
    let date = new Date(1970, 0, 1);
    date.setSeconds(sec);
    return date;
}

export default Article;
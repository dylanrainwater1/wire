import React, { Component } from 'react';

import './SideBar.css';

import { orderBy } from 'lodash';
import { SearchBox } from 'react-instantsearch-dom';

import { connectNumericMenu, connectRefinementList, connectCurrentRefinements } from 'react-instantsearch-dom';

class SideBar extends Component {
    constructor() {
        super();

        this.state = {
            count: 0
        }

        let now = Date.now();

        this.lastDay = new Date(now);
        this.lastDay.setDate(this.lastDay.getDate() - 1);
        this.lastDay = this.lastDay.getTime() / 1000;

        this.lastWeek = new Date(now);
        this.lastWeek.setDate(this.lastWeek.getDate() - 7);
        this.lastWeek = this.lastWeek.getTime() / 1000;

        this.lastThreeMonths = new Date(now);
        this.lastThreeMonths.setMonth(this.lastThreeMonths.getMonth() - 3);
        this.lastThreeMonths = this.lastThreeMonths.getTime() / 1000;

        this.lastSixMonths = new Date(now);
        this.lastSixMonths.setMonth(this.lastSixMonths.getMonth() - 6);
        this.lastSixMonths = this.lastSixMonths.getTime() / 1000;

        this.lastYear = new Date(now);
        this.lastYear.setFullYear(this.lastYear.getFullYear() - 1);
        this.lastYear = this.lastYear.getTime() / 1000;

        this.transformItems = this.transformItems.bind(this);
    }

    /*
        Provides order for RefinementList items and
        updates current count of items
    */
    transformItems(items) {
        let count = 0;
        items.forEach((item) => count += item.count);

        if (this.state.count !== count) {
            this.setState({ count });
        }

        return orderBy(items, ['label'], ['desc']);
    }

    render() {
        return (
            <div className="sidebar">
                <div className="mx-auto pt-3 search-panel">
                    <SearchBox searchAsYouType={false}
                        autoFocus
                        submit={<i className="fas fa-search" />}

                        reset={null}
                        translations={{
                            placeholder: "Search"
                        }}
                        className="px-0"
                        id="search-bar"
                    />
                </div>

                <div className="mx-auto mt-3 date-filters">
                    <h2>Show Articles From</h2>
                    <div className="row mx-auto">
                        <CustomNumericMenu
                            attribute="published.seconds"
                            items={[
                                { label: '24 hours', start: this.lastDay },
                                { label: '1 Week', start: this.lastWeek },
                                { label: '3 Months', start: this.lastThreeMonths },
                                { label: '6 Months', start: this.lastSixMonths },
                                { label: '1 Year', start: this.lastYear },
                            ]}
                        />
                    </div>
                </div>

                <hr />

                <div className="mx-0">
                    <div className="mx-auto tag-heading">
                        <h2>Filter {numberWithCommas(this.state.count)} Results</h2>
                        <CustomClearRefinements
                            translations={{
                                reset: 'Clear All',
                            }}

                        />
                    </div>

                    <div className="tag-container">
                        <div className="mx-auto tag-filters" id="tag-filters">
                            <div className="form-check checkbox checkbox-circle">
                                <CustomRefinementList attribute="source"
                                    transformItems={items => this.transformItems(items)}
                                    limit={999}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default SideBar;


const NumericMenu = ({ items, refine }) => {
    let half = Math.ceil(items.length / 2);
    let firstHalfItems = items.splice(0, half);
    let secondHalfItems = items;
    console.log('items: ', firstHalfItems);

    function clickHandler(e, item) {
        e.preventDefault();
        if (!item.noRefinement) {
            refine(item.value);
        }
    }

    function getClasses(item) {
        let classes = "btn btn-date";
        if (item.isRefined) {
            classes += " btn-date-selected";
        }

        return classes;
    }

    function renderItems(items) {
        return (
            <>
                {items.map(item => (
                    <button
                        key={item.value}
                        className={getClasses(item)}
                        onClick={e => clickHandler(e, item)}
                        disabled={item.noRefinement && !item.isRefined}>
                        {item.label}
                    </button>
                ))}
            </>);
    }

    return (
        <>
            <div className="col-sm">
                <div className="btn-group-vertical" role="group">
                    {renderItems(firstHalfItems)}
                </div>
            </div>

            <div className="col-sm">
                <div className="btn-group-vertical" role="group">
                    {renderItems(secondHalfItems)}
                </div>
            </div>
        </>
    );
};

let checkedRefs = null;

const RefinementList = ({
    items,
    refine,
}) => {
    if (!checkedRefs) {
        checkedRefs = {};
        items.forEach((item) => {
            checkedRefs[item.label] = false;
        })
    }

    return (
        <>
            {items.map(item => (
                <>
                    <input
                        className="form-check-input"
                        type="checkbox" value=""
                        id={`checkbox-${item.value}`}
                        checked={checkedRefs[item.label]}
                        readOnly
                    />
                    <label
                        className="form-check-label"
                        htmlFor={`checkbox-${item.value}`}
                        onClick={event => {
                            event.preventDefault();
                            checkedRefs[item.label] = !checkedRefs[item.label];
                            refine(item.value);
                        }}
                    >
                        {item.label}{' '}
                        <span className="tag-count">{item.count}</span>
                    </label><br />
                </>
            ))}
        </>
    );
}

const ClearRefinements = ({ items, refine, createURL }) => (
    <a onClick={() => {
        refine(items);
        checkedRefs = {};
    }}
        disabled={!items.length}
        className="clear-refinements"
        href={createURL(items)}
    >
        Clear All
    </a>
);


const CustomNumericMenu = connectNumericMenu(NumericMenu);
const CustomRefinementList = connectRefinementList(RefinementList);
const CustomClearRefinements = connectCurrentRefinements(ClearRefinements);